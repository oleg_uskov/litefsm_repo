import os
from inspect import getframeinfo, stack
import traceback


#x = Addr[35]

#OpregType = 0
#MuxPortType = 1
#InpREgType = 2

# Генерация массива размером 8х32
#opReg_bits_range = tuple( range(7, -1, -1))
#opReg = tuple((opReg_bits_range, "opreg") for i in range(0, 31))
def opReg(number : int):
    # def __getitem__(self, key):
    #     if isinstance(key, slice):
    #
    #         if (key.start < key.stop):
    #             raise ("Вектор всегда должен быть в направлении downTo, например, [7:0], а не [0:7]")
    #         opreg_tuple = opreg_tuple[key.start:(key.stop - 1):-1]  # это бутет соответсвовать формату (7 downto 0)
    #     else:
    #         raise Exception("Trying to access by index")
    return tuple(("opreg", number, i) for i in range(8))  # в  range задается количество бит




#wr_opReg_bits_range = tuple( (i, "wr_opreg") for i in range(8))
#wr_opReg = tuple(wr_opReg_bits_range for i in range(32))
# def wr_opReg(number : int):
#     return tuple(("wr_opreg", number, i) for i in range(8))

#muxPort = tuple((opReg_bits_range, "muxport") for i in range(0, 31))
def muxPort(number : int):
    return tuple(("muxport", number, i) for i in range(8))

# Генерация массива размером 64х2
#Reg32_bits_range = tuple(range(63, -1, -1))
#Reg32 = tuple((Reg32_bits_range, "Reg32") for i in range(2))
#wr_Reg32 = tuple((Reg32_bits_range, "wr_Reg32") for i in range(2))
#wr_outReg = tuple((Reg32_bits_range, "wr_outReg"))

def Reg32(number : int):
    return tuple(("Reg32", number, i) for i in range(32))

# def wr_Reg32(number : int):
#     return tuple(("wr_Reg32", number, i) for i in range(64))

# def wr_outReg(number : int):
#     return tuple(("wr_outReg", number, i) for i in range(64))
outReg =  tuple(("outReg", 0, i) for i in range(32))
#wr_outReg =  tuple(("wr_outReg", i) for i in range(64))

#
# def wr_addr(number : int, out_reg_len : int):
#     return tuple("wr_addr", number, out_reg_len)

def addr(number : int , reg = None):
    return "addr", number , reg

def lastAddrPlus(number : int ):
    return  number
###
def if_0(register):
    return "if_0", register

def ifnot0(register):
    return "ifnot0", register
###

# def outRegLen(number : int):
#     return tuple("outReglen", number)

# функция обвертка только для красоты, использовать не обязательно
def result(anyReg):
    return anyReg

zero_port = muxPort(0)

def var1(asm, number : int , reg = None):
    if (asm.last_load_addres_var1 != number):
        asm.Load(result(Reg32(1)), addr(number, reg))  #
        asm.last_load_addres_var1 = number
    return Reg32(1)

def var2(asm, number : int , reg = None):
    if (asm.last_load_addres_var2 != number):
        asm.Load(result(Reg32(2)), addr(number, reg))  #
        asm.last_load_addres_var1 = number
    return Reg32(2)


def debug_info():
    STsum = traceback.StackSummary
    Wst = traceback.walk_stack(None)
    Frame_info = STsum.extract(Wst, limit = 3)
    #print(Frame_info[2] , Frame_info[1] , Frame_info[0])
    return Frame_info[2][1], Frame_info[1][2], Frame_info[2][2] , Frame_info[2][0] # (номер строки, команда , состояние, файл)
    #caller = getframeinfo(stack()[1][0])
    #print ("%s:%d - %s" % (caller.filename, caller.lineno, message))



# декоратор для автомата состояния
def fsm_state(action):
    def wrapper_action(asm : AsmUtility):
        if (asm.initial_state_has_passed == 0):
            raise ("Первым нужно запускать initial_state")
        asm.Diagnostic_info[wrapper_action] = action.__name__

        label = asm.CreateLabel()
        action(asm)
        #asm.Goto_label(if_0(zero_port),label)
        asm.Wait(zero_port, 1)  # бесконечное ожидание в конце состояния

        # в конце каждой функции пытаемся запустить другую
        if (len(asm.FSMstateSet)):
            for FSMstate in asm.FSMstateSet:
                fsm_name = FSMstate            # выбираем первый элемент из множества
                break

            asm.Label_list[fsm_name] = asm.CreateLabel()
            asm.FSMstateSet.discard(fsm_name)
            fsm_name(asm)

        else: # значит что все функции уже были запущены
            # восстанавливаем метки
            for label, addr in asm.Goto_without_reference_List:
                empty_goto_command =  asm.CommandArray[addr]
                label_address : int =  asm.Label_list[label]
                empty_goto_command = empty_goto_command[0:8] + bin(label_address)[2:].zfill(16) + empty_goto_command[24:32]
                asm.CommandArray[addr] = empty_goto_command
            asm.write_command(256) # формируем файл с бинарным кодом
            asm.write_debug_line(256) # формируем файл с номерами строк вызываемыз команд

    return wrapper_action

# декоратор для начального состояния автомата
def initial_state(action):
    def wrapper_action(asm : AsmUtility):
        asm.Diagnostic_info[wrapper_action] = action.__name__ # информация для отладки - показывает имя функции, вложенную в декоратор
        asm.initial_state_has_passed = 1 # устанавливаем данный признак, чтобы первой запускался только initial state, а потом другие состояния
        label = asm.CreateLabel()
        asm.Label_list[wrapper_action] = label # устнавливаем метку для стартовой функции initial_state (чтобы не вызывать ее 2 раз)
        asm.Nop() # начальный адрес - всегда все нули
        action(asm)
        #asm.Goto_label(if_0(zero_port),label)
        asm.Wait(zero_port, 1) # бесконечное ожидание в конце состояния

        # в конце каждой функции пытаемся запустить другую
        if (len(asm.FSMstateSet)):
            for FSMstate in asm.FSMstateSet:
                fsm_name = FSMstate            # выбираем первый элемент из множества
                break

            asm.Label_list[fsm_name] = asm.CreateLabel()
            asm.FSMstateSet.discard(fsm_name)
            fsm_name(asm)


        else:  # значит что все функции уже были запущены
            # восстанавливаем метки
            for label, addr in asm.Goto_without_reference_List:
                empty_goto_command = asm.CommandArray[addr]
                label_address: int = asm.Label_list[label]
                empty_goto_command = empty_goto_command[0:8] + bin(label_address)[2:].zfill(16) + empty_goto_command[24:32]
                asm.CommandArray[addr] = empty_goto_command
            asm.write_command(256) # формируем файл с бинарным кодом
            asm.write_debug_line(256) # формируем файл с номерами строк вызываемыз команд
    return wrapper_action

# блок для цикла
def Loop(asm, register):
    start_loop = asm.CreateLabel()
    def loop_decor(*arg):
        if (register[0] == "if_0"):
            asm.Prior_goto_label( start_loop, if_0(register[1]),)  # крутимся в цикле пока регистр равен 0
        elif (register[0] == "ifnot0"):
            asm.Prior_goto_label(start_loop, ifnot0(register[1]), )  # крутимся в цикле пока регистр больше 0
        else:
            raise ("Неверный тип условия")

        # asm.Nop()
        # asm.Nop()

        FinishLoop_label = asm.CreateLabel()
        # восстанавливаем все Braek метки
        for addr in asm.Break_without_reference_List:
            empty_goto_command = asm.CommandArray[addr]
            empty_goto_command = empty_goto_command[0:8] + bin(FinishLoop_label)[2:].zfill(16) + empty_goto_command[24:32]
            asm.CommandArray[addr] = empty_goto_command

        asm.Break_without_reference_List.clear() # очищаем коллекцию для новых Break in Loop
    return loop_decor

# блок для ветвления в зависимоти от условия
def Switch(asm, register):

    if (register[0] == "if_0"):
        asm.Prior_goto_label("100000000undefined", ifnot0(register[1]) )  # переход к ElseSwitch_decor если услоние НЕ выполнилось
        Else_line_for_update = asm.CurrentAddress()
    elif (register[0] == "ifnot0"):
        asm.Prior_goto_label("100000000undefined", if_0(register[1]) )  # переход к ElseSwitch_decor если услоние НЕ выполнилось
        Else_line_for_update = asm.CurrentAddress()
    else:
        raise ("Неверный тип условия")
    def Switch_decor(*arg):
        # if (register[0] == "if_0"):
        #     asm.Goto_label(if_0(register[1]), "000000000undefined")  # переход через ElseSwitch_decor если услоние выполнилось
        #     Last_line_for_update = asm.CurrentAddress()
        # elif (register[0] == "ifnot0"):
        #     asm.Goto_label(ifnot0(register[1]), "000000000undefined")  # переход через ElseSwitch_decor если услоние выполнилось
        #     Last_line_for_update = asm.CurrentAddress()
        # else:
        #     raise ("Неверный тип условия")
        asm.Goto_label( "000000000undefined", if_0(zero_port),)  # безусловный переход через ElseSwitch_decor, потому что условие выполнилось
        Last_line_for_update = asm.CurrentAddress()
        ##восстанавливаем метку Else
        Else_label = asm.CreateLabel()
        empty_goto_command = asm.CommandArray[Else_line_for_update]
        empty_goto_command = empty_goto_command[0:8] + bin(Else_label)[2:].zfill(16) + empty_goto_command[24:32]
        asm.CommandArray[Else_line_for_update] = empty_goto_command



        def ElseSwitch_decor(*arg):
            ##восстанавливаем метку Last , то есть метку полсе последней команды Switch
            Last_label = asm.CreateLabel()
            empty_goto_command = asm.CommandArray[Last_line_for_update]
            empty_goto_command = empty_goto_command[0:8] + bin(Last_label)[2:].zfill(16) + empty_goto_command[24:32]
            asm.CommandArray[Last_line_for_update] = empty_goto_command

        return ElseSwitch_decor

    return Switch_decor

def Break(asm, register):
    asm.Goto_label( "000000000undefined", register,)  # переход в конец цикла
    asm.Break_without_reference_List.append(asm.CurrentAddress()) # запиминаем адрема с командани Break, которые нужно обновить в конце цикла Loop





class AsmUtility:

    # конструктор
    def __init__(self, FileName = "", PathToFile = "MIF"):
        self._file_name = FileName  # устанавливаем имя
        self._pathToFile = PathToFile
        #self.Label_list["Init"] = 0

    # флаг запуска состояния inital_state первым (защита от неправильной стартовой точки)
    initial_state_has_passed = 0
    last_load_addres_var1 = 0
    last_load_addres_var2 = 0

    #массив битовых строк
    CommandArray = []
    DedugCommand = []
    Label_list = {} # (LABEL,address)

    Goto_without_reference_List = [] # append address of command
    Break_without_reference_List = [] #

    FSMstateSet = set()

    Diagnostic_info = {}

    #init_state = 0


    def fsm_state (self, *args):
        for state_name in args:
            state_name()

########################### команды процессора  ######################
# команды процессора - логические
    def And(self, result, var1, var2):
        if (type(var1) == int):
            command = bin(3)[2:].zfill(6)
        else:
             command = bin(1)[2:].zfill(6)
        self.convert_asm_to_word_array("logic", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

    def Or(self, result, var1, var2):
        if (type(var1) == int):
            command = bin(4)[2:].zfill(6)
        else:
            command = bin(2)[2:].zfill(6)
        self.convert_asm_to_word_array("logic", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

    def Or_reduce(self, result, var1, var2):
        command = bin(7)[2:].zfill(6)
        self.convert_asm_to_word_array("logic", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

    def And_reduce(self, result, var1, var2):
        command = bin(8)[2:].zfill(6)
        self.convert_asm_to_word_array("logic", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

    def Concat(self, result, var2, var1):
        if (type(var1) == int):
            command = bin(11)[2:].zfill(6)
        else:
            command = bin(10)[2:].zfill(6)
        self.convert_asm_to_word_array("logic", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

# команды процессора - сравнения
    def Less(self, result, var2, var1):
        if (type(var1) == int):
            command = bin(20)[2:].zfill(6)
        else:
            command = bin(16)[2:].zfill(6)
        self.convert_asm_to_word_array("compare", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

    def More(self, result, var2, var1):
        if (type(var1) == int):
            command = bin(21)[2:].zfill(6)
        else:
            command = bin(17)[2:].zfill(6)
        self.convert_asm_to_word_array("compare", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

    def Equal(self, result, var2, var1):
        if (type(var1) == int):
            command = bin(22)[2:].zfill(6)
        else:
            command = bin(18)[2:].zfill(6)
        self.convert_asm_to_word_array("compare", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

    def NotEqual(self, result, var2, var1):
        if (type(var1) == int):
            command = bin(23)[2:].zfill(6)
        else:
            command = bin(19)[2:].zfill(6)
        self.convert_asm_to_word_array("compare", command ,result, var1, var2)
        self.DedugCommand.append(debug_info())

# команды процессора - присвоение
    def Move(self, result, var1,  var2 = if_0(zero_port)):
        if (var2[0] == "if_0"):
            if (type(var1) == int):
                command = bin(26)[2:].zfill(6)
            else:
                command = bin(25)[2:].zfill(6)
        elif (var2[0] == "ifnot0"):
            if (type(var1) == int):
                command = bin(28)[2:].zfill(6)
            else:
                command = bin(27)[2:].zfill(6)
        else:
            raise ("Неверный тип условия")
        self.convert_asm_to_word_array("move", command, result, var1, var2[1])
        self.DedugCommand.append(debug_info())

    def MoveInvers(self, result, var1,  var2 = if_0(zero_port)):
        command = bin(29)[2:].zfill(6)
        self.convert_asm_to_word_array("move", command, result, var1, var2[1][1])
        self.DedugCommand.append(debug_info())

    def InitAddr(self, var1):
        command = bin(24)[2:].zfill(6)
        self.convert_asm_to_word_array("init16", command, "undefined", var1)
        self.DedugCommand.append(debug_info())

# команды процессора - арифметические
    def Sum(self, result, var2, var1):
        if (type(var1) == int):
            command = bin(33)[2:].zfill(6)
        else:
            command = bin(32)[2:].zfill(6)
        self.convert_asm_to_word_array("arith", command, result, var1, var2)
        self.DedugCommand.append(debug_info())
    def Sub(self, result, var2, var1):
        if (type(var1) == int):
            command = bin(35)[2:].zfill(6)
        else:
            command = bin(36)[2:].zfill(6)
        self.convert_asm_to_word_array("arith", command, result, var1, var2)
        self.DedugCommand.append(debug_info())

# команды процессора - сдвиг
    def ShiftRight(self, result, var2, var1):
        command = bin(40)[2:].zfill(6)
        self.convert_asm_to_word_array("shift", command, result, var1, var2)

    def ShiftLeft(self, result, var2, var1):
        command = bin(41)[2:].zfill(6)
        self.convert_asm_to_word_array("shift", command, result, var1, var2)
        self.DedugCommand.append(debug_info())

# команды процессора - загрузка в память
    def Load(self, result, var1):
        if (type(var1) == int): # передается смещение относитльно последнего адреса
            command = bin(52)[2:].zfill(6)
            self.convert_asm_to_word_array("load", command, result, ("addr", var1 ), ("addr", var1))
        else:
            marker, addr , reg = var1
            if (reg == None):
                command = bin(51)[2:].zfill(6)
                self.convert_asm_to_word_array("load", command, result, (marker, addr ), (marker, addr))
            else:
                command = bin(53)[2:].zfill(6)
                self.convert_asm_to_word_array("load", command, result, (marker, addr), reg)

        self.DedugCommand.append(debug_info())

# команды процессора - выгрузка из памяти памяти
    def Store(self, result, var1):
        if (type(result) == int): # передается смещение относитльно последнего адреса
            command = bin(57)[2:].zfill(6)
            self.convert_asm_to_word_array("store", command, ("addr", result), var1, ("addr", result))
        else:
            marker, addr, reg = result
            if (reg == None):
                command = bin(56)[2:].zfill(6)
                self.convert_asm_to_word_array("store", command, (marker, addr ), var1, (marker, addr))
            else:
                command = bin(58)[2:].zfill(6)
                self.convert_asm_to_word_array("store", command, (marker, addr ), var1, reg)
        self.DedugCommand.append(debug_info())

# команды процессора - переход
    def Goto_label(self, var1, var2 = if_0(zero_port)):
        proir_move = "00"
        if (var2[0] == "if_0"):
            command = bin(61)[2:].zfill(6)
            if (var2[1] == zero_port): # данное условие говорит о безусловном перходе
                proir_move = "10"
        elif (var2[0] == "ifnot0"):
             command = bin(62)[2:].zfill(6)
        else:
            raise ("Неверный тип условия")
        self.convert_asm_to_word_array("goto", command, "undefined", var1, var2[1], proir_move)
        self.DedugCommand.append(debug_info())

    def Prior_goto_label(self, var1, var2 = if_0(zero_port)):
        if (var2[0] == "if_0"):
            command = bin(61)[2:].zfill(6)
        elif (var2[0] == "ifnot0"):
             command = bin(62)[2:].zfill(6)
        else:
            raise ("Неверный тип условия")
        self.convert_asm_to_word_array("goto", command, "undefined", var1, var2[1], "10")
        self.DedugCommand.append(debug_info())

    def Wait(self, var2, var1):
        command = bin(63)[2:].zfill(6)
        self.convert_asm_to_word_array("wait", command, opReg(0), var1, var2)
        self.DedugCommand.append(debug_info())

    def Nop(self):
        command = bin(0)[2:].zfill(6)
        self.convert_asm_to_word_array("load", command, Reg32(0), ("addr", 0 ), ("addr", 0 ))
        self.DedugCommand.append(debug_info())

#############
# вспомагательные команды
#     def End_programm(self):
#         self.write_command()

    def CreateLabel(self) -> int:
        return len(self.CommandArray)

    def CurrentAddress(self) -> int:
        return len(self.CommandArray)-1

    def Goto_state(self, state_label, var2 = if_0(zero_port)):
        proir_move = "00"
        if (var2[0] == "if_0"):
            command = bin(61)[2:].zfill(6)
            if (var2[1] == zero_port): # данное условие говорит о безусловном перходе
                proir_move = "10"
        elif (var2[0] == "ifnot0"):
             command = bin(62)[2:].zfill(6)
        else:
            raise ("Неверный тип условия")
        if (state_label.__name__ != 'wrapper_action'):
            raise ("переход возможен только в другое состояние автомата")

        if(self.Label_list.get(state_label, "none") != "none"):
            var1 = self.Label_list.get(state_label)
        else:
            #self.Label_list.append(state_label)
            self.Goto_without_reference_List.append((state_label, self.CurrentAddress() + 1)) ##(адресс команды, имя состояния) # делаем +1 , тк знаем что выполниеться еще команда convert_asm_to_word_array
            var1 = 0 # len(self.Label_list)
            self.FSMstateSet.add(state_label)   ## идет наполнение функций с отложеным вызовом

        self.convert_asm_to_word_array("goto", command, "undefined", var1, var2[1], proir_move)
        self.DedugCommand.append(debug_info())

######################################################
#### процедура формирования битовой команды команды
    def convert_asm_to_word_array (self, type_operation, command, result, var1 = "undefined", var2 = "undefined", var3 = "undefined",line = 0, label = 0,):
        if (type(result) == tuple):
            if (result[0][0] == "opreg"): # # многобитовый вектор
                bin_result = '0' + bin(result[0][1])[2:].zfill(6)
                lenght_result = abs(result[0][2] - result[-1][2]) + 1 # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
                start_bit_result = min(result[0][2], result[-1][2] - 1)
                if (start_bit_result > 0):
                    raise ("младший бит регистра должен быть 0")
                if (type_operation == "compare"):  #
                    raise ("результат сравнения должен быть всегда однобитовый")
                if (lenght_result == 8): # костыль из-за применения срезов
                    lenght_result = 7
                #bin_lenght = bin(lenght_result)[2:].zfill(3)
            elif (result[0] == "opreg"): # # однобитовый вектор
                numb_reg = result[1]
                start_bit_result = result[2]
                if (type_operation == "compare"): # bbbnnn - bbb номер бита оп.регистра, nnn - номер геристра
                     if (numb_reg > 7):  #
                        raise ("только в первые 8 регистров можно записать результат сравнения")
                     bin_result = '0' + bin(start_bit_result)[2:].zfill(3) + bin(numb_reg)[2:].zfill(3)
                else:
                    if (start_bit_result > 0):  #
                        raise ("в опер. регистры запись всегда должна начинаться с нулевого бита")
                    bin_result = '00' + bin(numb_reg)[2:].zfill(5)
                lenght_result = 0 #
                #bin_lenght = bin(lenght_result)[2:].zfill(3)
            elif (result[0][0] == "Reg32"): # многобитовый вектор
                if (type_operation == "load"):
                    bin_result = '1' + bin(result[0][1])[2:].zfill(2)  + '0000000'
                else:
                    lenght_result = abs(result[0][2] - result[-1][2]) + 1  # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
                    start_bit_result = min(result[0][2], result[-1][2] - 1)
                    bin_result = '10' + bin(start_bit_result)[2:].zfill(5)
                    if (lenght_result > 7): # костыль из-за применения срезов
                        raise ("длина должны быть не больше 8")

            elif (result[0] == "Reg32"): # однобитовый вектор
                bin_result = '10' + bin(result[2])[2:].zfill(5)
                lenght_result = 0
                #bin_lenght = bin(lenght_result)[2:].zfill(3)
            # elif (result[0][0] == "Reg32"): #wr_Reg32
            #     bin_result = '0' + bin(result[0][1])[2:].zfill(2)  + '0000000'
            elif (result[0] == "addr"): #wr_addr
                #bin_result =  bin(result[1])[2:].zfill(24)
                addr_16_8bit = result[1] >> 8 # берем только старшие 8 бит из 16 бит
                bin_result = bin(addr_16_8bit)[2:].zfill(8)
            else:
                raise ("Неверный тип")
        elif (type(result) == str): # для вывода ошибок
                bin_result = result
        else:
            raise ("Неверный тип")

        ####рассчет переменной var1
        if(type(var1) == tuple):
            if (var1[0][0] == "opreg"): # многобитовый вектор
                lenght_var1 = abs(var1[0][2] - var1[-1][2]) + 1  # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
                start_bit_var1 = min(var2[0][2], var1[-1][2] - 1)
                if (start_bit_var1 > 0):
                    raise ("младший бит регистра должен быть 0")
                if (lenght_var1 == 8): # костыль из-за применения срезов
                    lenght_var1 = 7
                if (var1[0][1] > 31):
                    raise ("значение должо быть от 0 до 31")
                bin_var1 = '000' + bin(var1[0][1])[2:].zfill(5)
            elif (var1[0] == "opreg"):# обнобитовый вектор
                lenght_var1 = 0
                if (var1[1] > 31):
                    raise ("значение должо быть от 0 до 31")
                bin_var1 = '000' + bin(var1[1])[2:].zfill(5)
            # elif (var1[0][0] == "muxport"):
            #     lenght_var1 = abs(var1[0][2] - var1[-1][2]) + 1  # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
            #     if (lenght_var1 = 8): # костыль из-за применения срезов
            #         lenght_var1 = 7
            #     if (var1[0][1] > 31):
            #         raise ("значение должо быть от 0 до 31")
            #     bin_var1 = '001' + bin(var1[0][1])[2:].zfill(5)
            # elif (var1[0] == "muxport"):
            #     lenght_var1 = 0
            #     if (var1[1] > 31):
            #         raise ("значение должо быть от 0 до 31")
            #     bin_var1 = '001' + bin(var1[1])[2:].zfill(5)
            elif (var1[0][0] == "Reg32" and result[0]== "addr"): #  команда сохранения в память
                bin_var1 = '0' +  bin(var1[0][1])[2:].zfill(2) + '0000000'
            elif (var1[0][0] == "Reg32"): # многобитовый вектор
                lenght_var1 = abs(var1[0][2] - var1[-1][2]) + 1  # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
                if (lenght_var1 > 7):
                    raise ("длина должны быть не больше 8")
                if (var1[0][1] > 3):
                    raise ("значение должо быть от 0 до 1")
                start_bit_var1 =  min(var1[0][2], var1[-1][2] - 1)
                bin_var1 = '1' +  bin(var1[0][1])[2:].zfill(2) + bin(start_bit_var1)[2:].zfill(5)
            elif (var1[0] == "Reg32"): # обнобитовый вектор
                lenght_var1 = 0
                if (var1[1]> 3):
                    raise ("значение должо быть от 0 до 1")
                start_bit_var1 =  var1[2]
                bin_var1 = '1' +  bin(var1[1])[2:].zfill(2)  + bin(start_bit_var1)[2:].zfill(5)
            elif (var1[0] == "addr"):
                addr_16_8bit = var1[1] >> 8 # берем только старшие 8 бит из 16 бит
                bin_var1 = bin(addr_16_8bit)[2:].zfill(8)

        elif (type(var1) == int):
            if (type_operation == "goto" or type_operation == "init16"):
                bin_var1 = var3 + bin(var1)[2:].zfill(16)
                if (var1 > 65536):
                    raise ("значение должо быть от 0 до 255")
            else : # обычная 8 битная константа
                lenght_var1 = 7
                if (var1 > 255):
                    raise ("значение должо быть от 0 до 255")
                bin_var1 = bin(var1)[2:].zfill(8)
        elif (type(var1) == str): # используется для команд переходов
                bin_var1 = var1
        else:
            raise ("Неверный тип")


        ####рассчет переменной var2
        if(type(var2) == tuple):
            if (var2[0][0] == "opreg"):# многобитовый вектор
                lenght_var2 = abs(var2[0][2] - var2[-1][2]) + 1  # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
                start_bit_var2 = min(var2[0][2], var2[-1][2] - 1)
                if (start_bit_var2 > 0):
                    raise ("младший бит регистра должен быть 0")
                if (lenght_var2 == 8): # костыль из-за применения срезов
                    lenght_var2 = 7
                if (var2[0][1] > 31):
                    raise ("значение должо быть от 0 до 31")
                bin_var2 = '000' + bin(var2[0][1])[2:].zfill(5)
            if (var2[0] == "opreg"): # обнобитовый вектор для первых 8 регистров
                lenght_var2 = 0
                if (var2[1] > 8):
                    raise ("значение должо быть от 0 до 7")
                bin_var2 = '01' + bin(var2[2])[2:].zfill(3) + bin(var2[1])[2:].zfill(3) # bbbnnn - bbb номер бита оп.регистра, nnn - номер геристра
            elif (var2[0][0] == "muxport"):# многобитовый вектор
                lenght_var2 = abs(var2[0][2] - var2[-1][2]) + 1  # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
                if (lenght_var2 == 8): # костыль из-за применения срезов
                    lenght_var2 = 7
                if (var2[0][1] > 31):
                    raise ("значение должо быть от 0 до 31")
                bin_var2 = '001' + bin(var2[0][1])[2:].zfill(5)
            elif (var2[0] == "muxport"):# обнобитовый вектор
                lenght_var2 = 0
                if (var2[1] > 31):
                    raise ("значение должо быть от 0 до 31")
                bin_var2 = '001' + bin(var2[0][1])[2:].zfill(5)
            elif (var2[0][0] == "Reg32"): # многобитовый вектор
                lenght_var2 = abs(var2[0][2] - var2[-1][2]) + 1  # рассчет дины входного вектора по разнице чисел и плюс 1 (из-за отличия Python от Verilog)
                if (lenght_var2 > 7):
                    raise ("длина должны быть не больше 8")
                if (var2[0][1] > 3):
                    raise ("значение должо быть от 0 до 1")
                start_bit_var2 =  min(var2[0][2], var2[-1][2] - 1)
                bin_var2 = '1' +  bin(var2[0][1])[2:].zfill(2) + bin(start_bit_var2)[2:].zfill(5)
            elif (var2[0] == "Reg32"): # обнобитовый вектор
                lenght_var2 = 0
                if (var2[1]> 3):
                    raise ("значение должо быть от 0 до 1")
                start_bit_var2 =  var2[2]
                bin_var2 = '1' +  bin(var2[1])[2:].zfill(2) + bin(start_bit_var2)[2:].zfill(5)
            elif (var2[0] == "addr"):
                addr_8bit = var2[1] & 0x000000ff  # берем только младшие 8 бит
                bin_var2 = bin(addr_8bit)[2:].zfill(8)
            # elif (var2[0][0] == "if_0"):
            #     if (var2[0][1] > 31):
            #         raise ("значение должо быть от 0 до 31")
            #     bin_var2 = '000' + bin(var2[0][1])[2:].zfill(5)
            # elif (var2[0][0] == "ifnot0"):
            #     if (var2[0][1] > 31):
            #         raise ("значение должо быть от 0 до 31")
            #     bin_var2 = '000' + bin(var2[0][1])[2:].zfill(5)
        # elif (type(var2) == int):
        #     if (var2 > 255):
        #         raise ("значение должо быть от 0 до 255")
        #     bin_var2 = bin(var2)[2:].zfill(8)
        elif (type(var2) == str): # для вывода ошибок
                bin_var2 = var2
        else:
            raise ("Неверный тип")

        ####рассчет длины операции
        if (type_operation == "compare" or type_operation == "wait") :
            if type(var1) == int :
                lenght_of_openard = lenght_var2  # длина 7+1 бит
            else:
                if (lenght_var1 != lenght_var2):
                    raise ("длина векторов должна быть одинакова")
                lenght_of_openard = lenght_var2  # длина 7+1 бит
        elif (type_operation == "logic" or
                 type_operation == "arith" or type_operation == "shift"):
            if type(var1) == int :
                if (lenght_var2 != lenght_result):
                    raise ("длина векторов должна быть одинакова")
                lenght_of_openard = lenght_var2  # длина 7+1 бит
            else:
                if (lenght_var1 != lenght_result and lenght_var1 != lenght_var2 ):
                    raise ("длина векторов должна быть одинакова")
                lenght_of_openard = lenght_var1  # длина 7+1 бит
        elif (type_operation == "move"): # эта команда с регистром условия, длину которого не стоит брать в расчет
            if type(var1) == int :
                lenght_of_openard = lenght_result  # длина 7+1 бит
            else:
                if (lenght_var1 != lenght_result):
                    raise ("длина векторов должна быть одинакова")
                lenght_of_openard = lenght_var1  # длина 7+1 бит
        else:
            lenght_of_openard = 0

        bin_lenght = bin(lenght_of_openard)[2:].zfill(3)

        ####рассчет битового кода
        if (type_operation == "logic" or type_operation == "compare" or
                type_operation == "move" or type_operation == "arith" or
                type_operation == "shift"):
            self.CommandArray.append(command + bin_result + bin_lenght + bin_var1 + bin_var2)
        elif (type_operation == "load" ):
            self.CommandArray.append(command + bin_result + bin_var1 + bin_var2)  #  Rin1  <=  RAM(addr);
        elif (type_operation == "store"):
            self.CommandArray.append(command + bin_var1 + bin_result + bin_var2)  #  RAM[addr] <= Rout(m:n);
        elif (type_operation == "goto"):
            self.CommandArray.append(command + bin_var1 + bin_var2)  #  If ( Rop2/Rin = ‘0’) PrCounter   <=   Const16;
        elif (type_operation == "init16"):
            self.CommandArray.append(command + "0000000000" + bin_var1)  #  addressReg   <=   Const16;
        elif (type_operation == "wait"):
            self.CommandArray.append(command + bin_result + bin_lenght + bin_var1 + bin_var2)  #
        else:
            raise ("Неверный тип операции")

        # self.Label_list.append((line, label, self.CommandArray[-1]))

    def write_command(self, quantity_line: int):
        if os.path.exists(self._pathToFile) == False:
            # путь относительно текущего скрипта
            os.mkdir(self._pathToFile)
            # print()
        cnt_line = 0
        with open(self._pathToFile + "/" + self._file_name + ".bin", "w") as binfile:
            # cnt_line += 1
            # binfile.write("00000000000000000000000000000000\n")
            for comm in self.CommandArray:
                cnt_line += 1
                if (cnt_line > quantity_line):
                    raise ("Количество команд больше чем " + str(quantity_line))
                binfile.write(comm)
                binfile.write('\n')
            while cnt_line < quantity_line:
                cnt_line +=  1
                binfile.write("00000000000000000000000000000000\n")

    def write_debug_line(self, quantity_line: int):
        if os.path.exists(self._pathToFile) == False:
            # путь относительно текущего скрипта
            os.mkdir(self._pathToFile)
            # print()
        cnt_line = 0
        with open(self._pathToFile + "/" + self._file_name + ".debug", "w") as binfile:
            for comm_info in self.DedugCommand:
                cnt_line += 1
                if (cnt_line > quantity_line):
                    raise ("Количество команд больше чем " + str(quantity_line))
                binfile.write(bin(comm_info[0])[2:].zfill(32) )
                binfile.write('\n')
            while cnt_line < quantity_line:
                cnt_line +=  1
                binfile.write("00000000000000000000000000000000\n")



# базовая структура для всех регистров
# class BaseRegister:
#     startBit = 0
#     stopBit = 7
#
#     # конструктор
#     def __init__(self, type, number,):
#         self._type = type
#         self._number = number
#
#     def rng (self, highBit, lowBit):
#         self.startBit = highBit
#         self.stopBit = lowBit
#         return self


