import Asm
from Asm import opReg, muxPort, Reg32, if_0, ifnot0 , addr, lastAddrPlus,  result, fsm_state, initial_state , Loop , Switch, zero_port, Break, var1, var2  #wr_accum, wr_Reg32, wr_Reg32, wr_opReg, accumLen,

dto  = -1
dto = dto
to  = 1
  
############# Карта размещения объектов #############

map_base_IO_addr_c          = 0x0000
p_data_stack_c              = 0x0100
p______c                    = 0x0200
p_logic_buf_c               = 0x0300
p_Cash_base_addr_c          = 0x1000

####################################################

############# Карта размещения подобъектов #############
zero_var_c                      = map_base_IO_addr_c + 0x0000
p_destination_c                 = map_base_IO_addr_c + 0x0001
map_base_InpUDPBuf_addr_c       = map_base_IO_addr_c + 0x0002
p_prev_LARGE_COUNTER            = map_base_IO_addr_c + 0x0003
p_prev_prev_LARGE_COUNTER       = map_base_IO_addr_c + 0x0004
p_number_station_c              = map_base_IO_addr_c + 0x0005
p_EXCHANGE_CONTROL_c            = map_base_IO_addr_c + 0x0006
p_secure_key_c                  = map_base_IO_addr_c + 0x0007


p_neigh1_A_package_c            = p_Cash_base_addr_c + 0x0000
p_neigh1_B_package_c            = p_Cash_base_addr_c + 0x0020
p_neigh2_A_package_c            = p_Cash_base_addr_c + 0x0040
p_neigh2_B_package_c            = p_Cash_base_addr_c + 0x0060
p_neigh3_A_package_c            = p_Cash_base_addr_c + 0x0080
p_neigh3_B_package_c            = p_Cash_base_addr_c + 0x00A0
p_neigh4_A_package_c            = p_Cash_base_addr_c + 0x00C0
p_neigh4_B_package_c            = p_Cash_base_addr_c + 0x00F0

############# Карта размещения переменных #############

####################################################

############# Объявление констант #############
# DSP_signature = 1
# SHN_signature = 2
# DC_signature = 3
# SH4I_signature = 4
# ether_exch_c = 0b00000011 # константа - обмен ethernet пакетами
####################################################

############# Объявление переменных #############
accum = Reg32(0)

zero_flag                = zero_port
receivedUDP_flag         = muxPort(1)
emptyUDPbuf_flag         = muxPort(2)
# destination_high       = muxPort(3)
# destination_low        = muxPort(4)
#EXCHANGE_CONTROL_INP    = muxPort(5)
cnt_down                 = muxPort(6)

condition_reg            = opReg(0)
error_received_command   = opReg(1)
received_buf             = opReg(2)

counter                  = opReg(4)
received_statID_lowB     = opReg(5)
received_statID_highB    = opReg(6)




####################################################

############# Установка имени файла конфигурации #############
asm = Asm.AsmUtility("ParsingARMPackage","MIF/ETHERNET")
####################################################

############# Начальное состояние овтомата #############
@initial_state
def Init(asm):
    asm.Move(result(condition_reg), 0) # ошищаем регистр с условием
    asm.Move(result(error_received_command), 0) # ошищаем регистр ошибок
    Loop(asm, if_0(zero_flag))(
        asm.Load(result(Reg32(1)), addr(p_EXCHANGE_CONTROL_c)),  #
        asm.Equal(result(condition_reg[0]), Reg32(1)[7:0:dto], 0),  # ждем новые данные
        asm.Goto_state(reload_command_data, ifnot0(condition_reg[0]), ),

        asm.Equal(result(condition_reg[0]), receivedUDP_flag, 1),  # ждем новые данные
        asm.Goto_state(head_parser, ifnot0(condition_reg[0]), ),

    )



############# Состояние овтомата - разбор заголовка пакета #############
@fsm_state
def head_parser (asm):
    asm.Move(result(condition_reg), 0) #
    #asm.Wait(emptyUDPbuf_flag, 0)  # флаг непустого буфера
    asm.Load(result(Reg32(1)), addr(map_base_InpUDPBuf_addr_c)) # загрузка 1 слова
    #asm.Load(result(Reg32(1)), addr(map_base_Contant_32b_Buf_c))
    asm.Equal(result(condition_reg[0]), Reg32(1)[7:0:dto], ord('c'))
    asm.Equal(result(condition_reg[1]), Reg32(1)[15:8:dto], ord('t'))
    asm.Equal(result(condition_reg[2]), Reg32(1)[23:16:dto], ord('b'))
    asm.Equal(result(condition_reg[3]), Reg32(1)[31:24:dto], ord('a'))
    asm.NotEqual(result(error_received_command[0]), condition_reg[3:0:dto], 0b1111) # проверяем корректность
    asm.Store(result(addr(p_data_stack_c)), Reg32(1))  # сохраняем 1 слово пакета

    asm.Load(result(Reg32(1)), addr(map_base_InpUDPBuf_addr_c)) # загрузка 2 слова (Source + Dest )
    asm.Move(result(accum[7:0:dto]), Reg32(1)[23:16:dto])
    asm.Move(result(accum[15:8:dto]), Reg32(1)[31:24:dto])
    asm.Store(result(addr(p_number_station_c)), accum)  #  отправка данных(станция-источник) для определения номера буфера
    asm.Load(result(Reg32(2)), addr(p_destination_c)) # загрузка перемычек
    asm.Equal(result(condition_reg[0]), Reg32(1)[7:0:dto], Reg32(2)[7:0:dto])
    asm.Equal(result(condition_reg[1]), Reg32(1)[15:8:dto], Reg32(2)[15:8:dto])
    asm.NotEqual(result(error_received_command[1]), condition_reg[1:0:dto], 0b11) # проверяем корректность
    asm.Store(result(addr(p_data_stack_c)), Reg32(1))  #

    asm.Load(result(Reg32(1)), addr(map_base_InpUDPBuf_addr_c)) # загрузка 3 слова (Counter)
    asm.Store(result(addr(p_data_stack_c)), Reg32(1))  #

    # asm.Load(result(Reg32(1)), addr(map_base_InpUDPBuf_addr_c)) # загрузка 4 слова (Receipt)
    # asm.Load(result(Reg32(2)), addr(p_prev_LARGE_COUNTER)) # загрузка текущего счетчик
    asm.Equal(result(condition_reg[0]), var1(asm,map_base_InpUDPBuf_addr_c)[7:0:dto],   var2(asm,p_prev_LARGE_COUNTER)[7:0:dto])
    asm.Equal(result(condition_reg[1]), var1(asm,map_base_InpUDPBuf_addr_c)[15:8:dto],  var2(asm,p_prev_LARGE_COUNTER)[15:8:dto])
    asm.Equal(result(condition_reg[2]), var1(asm,map_base_InpUDPBuf_addr_c)[23:16:dto], var2(asm,p_prev_LARGE_COUNTER)[23:16:dto])
    asm.Equal(result(condition_reg[3]), var1(asm,map_base_InpUDPBuf_addr_c)[31:24:dto], var2(asm,p_prev_LARGE_COUNTER)[31:24:dto])
    asm.NotEqual(result(error_received_command[3]), condition_reg[3:0:dto], 0b1111) # проверяем корректность квитанции

    asm.Load(result(Reg32(1)), addr(map_base_InpUDPBuf_addr_c)) #загрузка 5 слова (version + SizeInDw)
    asm.Move(result(counter), Reg32(1)[7:0:dto]) # сохраняем длину данных (размер в 32битных словах)

    Switch(asm, ifnot0(var1(asm,p_number_station_c)[0]))( # сложить в первый буфер
        asm.InitAddr(p_neigh1_A_package_c)
    )(
        Switch(asm, ifnot0(var1(asm,p_number_station_c)[1]))( # сложить во второй буфер
            asm.InitAddr(p_neigh1_B_package_c)
        )(
            Switch(asm, ifnot0(var1(asm, p_number_station_c)[2]))(  # сложить в 3 буфер
                asm.InitAddr(p_neigh2_A_package_c)
            )(
                Switch(asm, ifnot0(var1(asm, p_number_station_c)[3]))(  # сложить в 4 буфер
                    asm.InitAddr(p_neigh2_B_package_c)
                )(
                    Switch(asm, ifnot0(var1(asm, p_number_station_c)[4]))(  # сложить в 5 буфер
                        asm.InitAddr(p_neigh3_A_package_c)
                    )(
                        Switch(asm, ifnot0(var1(asm, p_number_station_c)[5]))(  # сложить в 6 буфер
                            asm.InitAddr(p_neigh3_B_package_c)
                        )(
                            Switch(asm, ifnot0(var1(asm, p_number_station_c)[6]))(  # сложить в 7 буфер
                                asm.InitAddr(p_neigh4_A_package_c)
                            )(
                                Switch(asm, ifnot0(var1(asm, p_number_station_c)[7]))(  # сложить в 8 буфер
                                    asm.InitAddr(p_neigh4_B_package_c)
                                )(),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
#########

    asm.Store(result(lastAddrPlus(1)), var1(asm,p_data_stack_c))  # пересохраняем 1 слово
    asm.Store(result(lastAddrPlus(1)), var1(asm,p_data_stack_c))  # пересохраняем 2 слово
    asm.Store(result(lastAddrPlus(1)), var1(asm,p_data_stack_c))  # пересохраняем 3 слово
    asm.Store(result(lastAddrPlus(1)), var1(asm,p_data_stack_c))  # пересохраняем 4 слово
    asm.Store(result(lastAddrPlus(1)), var1(asm,p_data_stack_c))  # пересохраняем 5 слово

    Loop(asm, ifnot0(counter))( # сохраняем данные
        asm.Load(result(Reg32(1)), addr(map_base_InpUDPBuf_addr_c)),  #
        asm.Store(result(addr(p_data_stack_c)), Reg32(1)),
        asm.Nop(),
        asm.Nop(),
        asm.Sub(result(counter), counter, 1),
    )
    asm.Move( result(counter), 32) # сохраняем длину хэш суммы(размер в 32битных словах)
    Loop(asm, ifnot0(counter))( #
        asm.Load(result(Reg32(1)), addr(map_base_InpUDPBuf_addr_c)),  # cчитываем команду
        asm.Store(result(addr(p_data_stack_c)), Reg32(1)),
        asm.Nop(),
        asm.Nop(),
        asm.Sub(result(counter), counter, 1),
    )
    #asm.Goto_state(Finish, ifnot0(error_received_command))

    asm.Goto_state(save_data_field)

############# Состояние овтомата - сохранение команды от ДЦ #############
@fsm_state
def save_data_field(asm):
    asm.Goto_state(check_CRC)


############# Состояние овтомата - целостности пакета #############
@fsm_state
def check_CRC(asm):
    asm.Load(result(Reg32(1)), addr(0)) # cчитываем команду
    asm.Load(result(Reg32(0)), addr(map_base_InpUDPBuf_addr_c)) # cчитываем команду
    asm.Equal(result(condition_reg[0]) , Reg32(0)[7:0:dto], Reg32(1)[31:24:dto])    # сравниваем первый байт CRC

    asm.NotEqual(result(error_received_command[4]), condition_reg[3:0:dto], 0b1111 )  # фиксируем ошибку CRC


    # asm.Move(result(accum[0]), 0),  # сбрасываем флаг разрешения на подсчет CRC
    # asm.Store(addr(p_En_CRC_calc_c), accum) # сбрасываем флаг разрешения на подсчет CRC
    asm.Goto_state(Finish)

############# Состояние овтомата - окончане приема, проверка опустощения буфера #############
@fsm_state
def Finish(asm):
    # asm.Move(result(accum[0]), 0),  # сбрасываем флаг разрешения на подсчет CRC
    # asm.Store(addr(p_En_CRC_calc_c), accum) # сбрасываем флаг разрешения на подсчет CRC
    asm.Nop();
    Switch(asm, if_0(emptyUDPbuf_flag))( # если остался мусор, то чистим его
        Loop(asm, if_0(emptyUDPbuf_flag))(  # то идет очистка мусора
            asm.Load(result(Reg32(0)), addr(map_base_InpUDPBuf_addr_c)),  # загружаем муссор
            asm.Load(result(Reg32(0)), addr(map_base_InpUDPBuf_addr_c)),  # загружаем муссор
        ),
     )( ),
    asm.Store(result(addr(0)), accum),  # флаг завершения разбора пакета
    # Switch(asm, if_0(error_received_command))(  # сохраняеем, только если не было ошибок
    #     Switch(asm, ifnot0(counter))(  # перегружаем, только если есть признак наличия команды
    #         asm.Move(result(counter), 8),
    #         asm.Load(result(accum), addr(0)),  # cчитываем команду из промежуточного буфера
    #         asm.Store(addr(0, counter), accum),  #
    #         Loop(asm, ifnot0(condition_reg[0]))(  # готовим валидную команду для выравнивания
    #             asm.Sum(result(counter), counter, 1),  #
    #             asm.Load(result(accum), lastAddrPlus(1)),  #
    #             asm.Store(addr(0, counter), accum),  #
    #             asm.Less(result(condition_reg[0]), counter, 13),
    #         )
    #     )( ),
    #
    # )( ),
    asm.Goto_state(Init)



############# Состояние овтомата - перегрузка и выравнивание команд #############
@fsm_state
def reload_command_data (asm):
    asm.Move(result(condition_reg), 0) # ошищаем регистр с условием
    asm.Move(result(counter), 8)
    Loop(asm, ifnot0(condition_reg[0]))( # перегружаем весь буфер

        asm.Sum(result(counter), counter, 1),
        asm.Less(result(condition_reg[0]), counter, 13)
    )

    asm.Goto_state(SendCommandToAnalize)




############# Состояние овтомата - передача комады для детального разбора содержимого #############
@fsm_state
def SendCommandToAnalize(asm):

    asm.Load(result(accum), addr(zero_var_c)),  # очищаем весь регистр
    asm.Move(result(counter), 8),
    Loop(asm, ifnot0(condition_reg[0]))(# # очищаем приемный буфер в цикле
        asm.Store(addr(0, counter), accum),  # очищаем приемный буфер
        asm.Sum(result(counter), counter, 1),  #
        asm.Less(result(condition_reg[0]), counter, 13),
    )

    asm.Goto_state(Init)